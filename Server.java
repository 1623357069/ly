/**
 * @基本功能:
 * @ClassName: Socket
 * @author: ly
 * @create: 2021-09-10 15:35:25
 **/

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String args[]) throws IOException {
//创建一个服务器端Socket，即ServerSocket，指定绑定的端口，并监听此端口
        ServerSocket serverSocket = new ServerSocket(80);
//调用accept()方法开始监听，等待客户端的连接
        Socket socket = serverSocket.accept();
//获取输入流，并读取客户端信息
        InputStream is = socket.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String info = null;
//        while((info = br.readLine()) != null){
//            System.out.println("我是server，client说：" + info);
//        }
        info = br.readLine();
        String[] date = info.split("\\ ");
        socket.shutdownInput();//关闭输入流
        String data = date[1].substring(1);
        //最终的值
        int sum = 0;
        //是否正确获得到最终的值
        boolean flag = false;
        if (data.substring(0, 3).equals("add")) {
            if (data.charAt(3) == '?') {
                String x = data.substring(3);
                String[] y = x.split("&");
                int a = Integer.valueOf(y[0].split("=")[1]);
                int b = Integer.valueOf(y[1].split("=")[1]);
                sum = a + b;
                flag = true;
            }
        } else if (data.substring(0, 4).equals("mult")) {
            if (data.charAt(4) == '?') {
                String x = data.substring(4);
                String[] y = x.split("&");
                int a = Integer.valueOf(y[0].split("=")[1]);
                int b = Integer.valueOf(y[1].split("=")[1]);
                sum = a * b;
                flag = true;
            }
        }
        if (flag)
            System.out.println(sum);
//获取输出流，响应客户端的请求
        OutputStream os = socket.getOutputStream();
        PrintWriter out = new PrintWriter(os);
        out.println();
        // 输出请求资源
        out.println(sum);
        out.close();

//清除缓冲
        os.flush();

//关闭链接
        br.close();
        isr.close();
        is.close();
        socket.close();
        serverSocket.close();
    }

}



